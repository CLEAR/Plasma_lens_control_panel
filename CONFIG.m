function [ config ] = CONFIG()
    
    %% PLE GUI CONFIG

    % initialize struct
    config = struct();
    
    % capillary
    config.cap.diameter = 1000; % [um]
    config.cap.radius = config.cap.diameter/2;
    config.cap.w_sapph = 4000; % [um]
    config.cap.h_sapph = 10000; % [um]
    
    % mover
    config.mover.x_ref_out = 66600; % [steps]
    config.mover.x_ref_in = 73350; % [steps]
    config.mover.y_ref = 194000; % [steps]
    config.mover.cal_x = 1.000; % [steps/um]
    config.mover.cal_y = 2.610; % [steps/um]
    config.mover.hor_name = 'CA.MOV0800-H';
    config.mover.hor_target = [config.mover.hor_name '/AQN#targetPosition'];
    config.mover.hor_actual = [config.mover.hor_name '/AQN#actualPosition'];
    config.mover.hor_request = [config.mover.hor_name '/CMD#requestedPosition'];
    config.mover.vert_name = 'CA.MOV0800-V';
    config.mover.vert_target = [config.mover.vert_name '/AQN#targetPosition'];
    config.mover.vert_actual = [config.mover.vert_name '/AQN#actualPosition'];
    config.mover.vert_request = [config.mover.vert_name '/CMD#requestedPosition'];
    
    % GB edits
    config.mover.cal_x_10um = 0.716; %[steps/um]  
    config.mover.cal_x_25um = 0.904;
    config.mover.cal_x_50um = 0.930;
    config.mover.cal_y_10um = 2.264;
    config.mover.cal_y_25um = 2.379;
    config.mover.cal_y_50um = 2.435;
    
    
    % discharge trigger
    config.trigger.switch = 'CL.ECLCNT4/Enabled#enabled';
    config.trigger.timing = 'CL.ECLCNT4/CoarseDelay#delay';
    
    % camera trigger
    config.camgate.switch = 'CL.ECLCNT18/Enabled#enabled';
    config.camgate.timing = 'CL.ECLCNT18/CoarseDelay#delay';
    
    % scopes
    config.scopes.current_in = 'CA.SCOPE11.CH02';
    % config.scopes.cal_AperV_in = 2*20*1.067*10; % [A/V]
    config.scopes.cal_AperV_in = 410.9; % [A/V]
    config.scopes.current_out = 'CA.SCOPE11.CH03';
    % config.scopes.cal_AperV_out = 2*20*1.067*10; % [A/V]
    config.scopes.cal_AperV_out = 410.9; % [A/V]
    config.scopes.PMT = 'CA.SCOPE11.CH04';
    config.scopes.PMT_fudgefactor = 200;
    
    % history waterfall
    config.history.max_shots = 200;
    config.history.current_limits = [-400, 1600];
    
    % periodic on/off (defaults)
    config.periodic.numshots_on = 1;
    config.periodic.numshots_off = 0;
    
    % cameras
    config.cams.capillary = 'CA.BTV0800';
    config.cams.electrode_OTR = 'CA.BTV0805';
    config.cams.plasma_OTR = 'CA.BTV0810';
    config.cams.in_air_YAG = 'CA.BTV0910';
    %Digital sensors ('N/A' for analog cams)
    config.digicams.capillary     = 'N/A';
    config.digicams.electrode_OTR = 'N/A';
    config.digicams.plasma_OTR    = 'CA.BTV0810.DigiCam';
    config.digicams.in_air_YAG    = 'CA.BTV0910.DigiCam';
    
    % lamps
    config.lamps.capillary = 'secondLamp';
    config.lamps.plasma_OTR = 'firstLamp';
    config.lamps.in_air_YAG = 'secondLamp';
    config.lamps.electrode_OTR = 'secondLamp';
    
    % links
    config.links.DAQ = '/clear/data/MatLab/Experiments/PlasmaLens/clindstr/CLEAR_DAQ/DAQ';
    
    % gains
    config.gains.text = {'Gain x1', 'Gain x2.2', 'Gain x6.5', 'Gain x8'};
    config.gains.vals = {'X_ONE', 'X_TWO_DOT_TWO', 'X_SIX_DOT_FIVE', 'X_EIGHT'};
    
    % PositionGauge
    %config.gauge.host = 'clear-mitutoyo-clic-structure.cern.ch';
    config.gauge.host = 'clear-mitutoyo-plasma-lens.cern.ch';
    
    config.gauge.xport = 21;
    config.gauge.yport = 22;
end

