 function varargout = PLE_GUI(varargin)

    % add utilities
    path = strrep(mfilename('fullpath'), ['/' mfilename], '/');
    addpath([path 'utilities']);
    
    % PLE_GUI MATLAB code for PLE_GUI.fig
    %      PLE_GUI, by itself, creates a new PLE_GUI or raises the existing
    %      singleton*.
    %
    %      H = PLE_GUI returns the handle to a new PLE_GUI or the handle to
    %      the existing singleton*.
    %
    %      PLE_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in PLE_GUI.M with the given input arguments.
    %
    %      PLE_GUI('Property','Value',...) creates a new PLE_GUI or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before PLE_GUI_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to PLE_GUI_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES

    % Edit the above text to modify the response to help PLE_GUI

    % Last Modified by GUIDE v2.5 21-Oct-2019 11:54:44

    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @PLE_GUI_OpeningFcn, ...
                       'gui_OutputFcn',  @PLE_GUI_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT


% --- Executes just before PLE_GUI is made visible.
function PLE_GUI_OpeningFcn(hObject, eventdata, handles, varargin)

    % to incaify MATLAB!
    try
        matlabJapc.staticINCAify('CTF');
        %disp('Not INCAified!!!');
    catch e
        if isempty(strfind(e.message, 'InCA client was already configured'))
            % display error if not just that it was already InCAified
            display(e);
            display(e.message);
        end
    end
    
    config = CONFIG();
    
    % Choose default command line output for PLE_GUI
    handles.output = hObject;
    
    % initialize plots
    handles = setupMoverPlot(handles);
    handles = setupDischargePlot(handles);
    handles = setupDischargeHistoryPlots(handles);
    handles = setupCameraPlots(handles);
    
    % make a gui refresh timer
    refreshRate = 10; % [Hz]
    handles.refreshTimer = timer('Name', 'RefreshTimer', ...
                                 'Period', 1/refreshRate, ...
                                 'TasksToExecute', Inf, ...
                                 'ExecutionMode', 'fixedSpacing', ...
                                 'timerFcn', {@refreshTimerCallback, handles.moverPlot});
    
    % make a subscription for the scopes
    handles.scopesMonitor = matlabJapcMonitor('SCT.USER.SETUP', ...
                                              {[config.scopes.current_in '/Acquisition'], ...
                                               [config.scopes.current_out '/Acquisition'], ...
                                               [config.scopes.PMT '/Acquisition']}, ...
                                              @(shot,mObj)shotCallback(shot,mObj,handles), ...
                                              'PLE GUI shots');
    
    % make a separate subscription for cameras (to collect scopes and cams simulatenously)
    handles.camsMonitor = matlabJapcMonitor('SCT.USER.SETUP', ...
                                            {[config.cams.capillary '/Image'], ...
                                             [config.cams.electrode_OTR '/Image'] ...
                                             ... %[config.cams.plasma_OTR '/Image'], ...
                                             ... %[config.digicams.plasma_OTR '/ExtractionImage'], ...
                                             ... %[config.cams.in_air_YAG '/Image']}, ...
                                             ... %[config.digicams.in_air_YAG '/ExtractionImage']}, ...
                                             },...
                                            @(shot,mObj)camShotCallback(shot,mObj,handles), ...
                                            'PLE GUI camera shots');
    
    handles.digiCamsMontitor = matlabJapcMonitor('', ...
                                            {[config.digicams.plasma_OTR '/ExtractionImage'], ...
                                             [config.digicams.in_air_YAG '/ExtractionImage']}, ...
                                            @(shot,mObj)camShotCallback_digital(shot,mObj,handles), ...
                                            'PLE GUI digital camera shots');
                                        
    handles.positionGaugeConnectorX = positionGaugeConnector(config.gauge.host, config.gauge.xport);
    handles.positionGaugeConnectorY = positionGaugeConnector(config.gauge.host, config.gauge.yport);
    
    posGaugeRefreshRate = 2; % [Hz]
    handles.posGaugeRefreshRate = posGaugeRefreshRate;
    handles.posGaugeRefreshTimer = timer('Name', 'PosGaugeRefreshTimer', ...
                                         'Period', 1/posGaugeRefreshRate, ...
                                         'TasksToExecute', Inf, ...
                                         'ExecutionMode', 'fixedSpacing', ...
                                         'timerFcn', {@posGaugeRefreshTimerCallback, handles.moverPlot});
    handles.lastGaugeTimeX = 0;
    handles.lastGaugeTimeY = 0;
                             
    % Update handles structure
    guidata(hObject, handles);
    
    % start the timers
    start(handles.refreshTimer);
    start(handles.scopesMonitor);
    start(handles.camsMonitor);
    start(handles.digiCamsMontitor);
    start(handles.posGaugeRefreshTimer)


% --- Outputs from this function are returned to the command line.
function varargout = PLE_GUI_OutputFcn(hObject, eventdata, handles)
    varargout{1} = handles.output;
    
    
% user closes the window: stop the timers
function figure1_CloseRequestFcn(hObject, ~, handles)
    % stop the timer and monitors
    try
        stop(handles.refreshTimer);
    catch e
        warning('could not stop handles.refreshTimer')
    end
    
    try 
        stop(handles.scopesMonitor);
    catch e
        warning('could not stop handles.scopesMonitor')
    end
    
    try
        stop(handles.camsMonitor);
    catch e
        warning('could not stop handles.camsMonitor')
    end
    
    try
        stop(handles.digiCamsMontitor)
    catch e
        warning('could not stop handles.digiCamsMontitor')
    end
    
    try
        stop(handles.posGaugeRefreshTimer)
    catch e
        warning('could not stop handles.posGaugeRefreshTimer')
    end
    clear handles.positionGaugeConnectorX
    clear handles.positionGaugeConnectorY
    
    % delete the figure
    delete(hObject);


% everything that should be updated (plots etc)
function refreshTimerCallback(~, ~, guiHandle)
    if ~isempty(guiHandle)
        handles = guidata(guiHandle);
        if ~isempty(handles)
            updateMoverPlot(handles);
            updateDischargeStatus(handles);
            %updateGatedCameraStatus(handles);
        end
    end

% Update position gauge data
function posGaugeRefreshTimerCallback(~, ~, guiHandle)
    if ~isempty(guiHandle)
        handles = guidata(guiHandle);
        if ~isempty(handles)
            updateMoverPlot_gauges(handles);
        end
    end

    
% update scopes and more every time there is a beam shot
function shotCallback(shot, ~, handles)
    updateDischargePlot(handles, shot);
    updateDischargeHistoryPlots(handles, shot);
    periodicTurnDischargeOnOff(handles);
    
    
% update cameras every time there is a beam shot
function camShotCallback(shot, ~, handles)
    updateCameras(handles, shot, false);
    updateGainMenus(handles);
    updateAllCameraOnOffToggles(handles);
    updateAllLampOnOffToggles(handles)
    updateAllLampSliders(handles)

function camShotCallback_digital(shot, ~, handles)
    updateCameras(handles, shot, true)
    
function periodicTurnDischargeOnOff(handles)
    count = handles.periodicOnOffPanel.UserData;
    numshots_on = str2double(handles.periodicNumShotsONedit.String);
    numshots_off = str2double(handles.periodicNumShotsOFFedit.String);
    % reset counter if too high
    if count > numshots_on + numshots_off
        count = 1;
        handles.periodicOnOffPanel.UserData = count;
    end
    % turn on or off
    if count <= numshots_on
        % keep on
        if handles.dischargeOnOffToggle.Value == 1
            turnDischargeON();
        else
            turnDischargeOFF();
        end
    else
        % turn off
        turnDischargeOFF();
    end
    % increment the counter
    handles.periodicOnOffPanel.UserData = count + 1;

    
function handles = setupMoverPlot(handles)
    config = CONFIG();
    
    % set up the plots
    axes(handles.moverPlot);
    handles.moverPlot_cursor = plot(0, 0, '+', 'Color', [0.85 0.325 0.098]);
    hold on;
    handles.moverPlot_sapphire = fill(NaN, NaN, [0.98 0.98 1]);
    handles.moverPlot_cap = fill(NaN, NaN, 'w', 'EdgeColor', [0 0.447 0.741]);
    handles.moverPlot_outline = plot(NaN, NaN, '-', 'Color', handles.moverPlot_cap.EdgeColor);
    handles.moverPlot_cap_req = plot(NaN, NaN, ':', 'Color', 0.7*[1 1 1]);
    handles.moverPlot_sapph_req = plot(NaN, NaN, ':', 'Color', handles.moverPlot_cap_req.Color);
    hold off;
    uistack(handles.moverPlot_cursor, 'top');
    
    % make em shiny
    ndiam = 1.3;
    ylim(ndiam*config.cap.diameter*[-1 1]);
    xlabel('x (\mum)');
    ylabel('y (\mum)');
    axis equal;
    xlim(get(gca,'xlim'));
    
    % set the mover reference to default
    handles.moverReference.x_ref = config.mover.x_ref_in;
    handles.moverReference.y_ref = config.mover.y_ref;
    
    % fill the mover reference edit fields
    set(handles.moverHorRefEdit, 'String', num2str(handles.moverReference.x_ref));
    set(handles.moverVertRefEdit, 'String', num2str(handles.moverReference.y_ref));
    
    
function updateMoverPlot(handles)
    config = CONFIG();
    
    % get current and requested mover positions
    x_pos = getVariable(config.mover.hor_actual); % [steps]
    y_pos = getVariable(config.mover.vert_actual); % [steps]
    x_target = getVariable(config.mover.hor_target); % [steps]
    y_target = getVariable(config.mover.vert_target); % [steps]
    dx = (x_pos-handles.moverReference.x_ref)/config.mover.cal_x;
    dy = (y_pos-handles.moverReference.y_ref)/config.mover.cal_y;
    dx_req = (x_target-handles.moverReference.x_ref)/config.mover.cal_x;
    dy_req = (y_target-handles.moverReference.y_ref)/config.mover.cal_y;
    
    % make capillary ring
    thetas = linspace(0,2*pi,200);
    xs = dx + config.cap.radius*cos(thetas);
    ys = dy + config.cap.radius*sin(thetas);
    xs_req = dx_req + config.cap.radius*cos(thetas);
    ys_req = dy_req + config.cap.radius*sin(thetas);
    
    % update the mover plot
    set(handles.moverPlot_cap, 'XData', xs, 'YData', ys);
    set(handles.moverPlot_outline, 'XData', dx+config.cap.w_sapph*[0 0 1 1 0 0], 'YData', dy+[-config.cap.radius config.cap.h_sapph*[-1 -1 1 1] config.cap.radius]);
    set(handles.moverPlot_sapphire, 'XData', dx+config.cap.w_sapph*[1 1 -1 -1], 'YData', dx+config.cap.h_sapph*[-1 1 1 -1]);
    set(handles.moverPlot_cap_req, 'XData', xs_req, 'YData', ys_req);
    set(handles.moverPlot_sapph_req, 'XData', dx_req+config.cap.w_sapph*[0 0 1 1 0 0], 'YData', dy_req+[-config.cap.radius config.cap.h_sapph*[-1 -1 1 1] config.cap.radius]);
    
    % update status text
    set(handles.horMoverTextMetric, 'String', [num2str(round(dx)) ' um']);
    set(handles.horMoverTextSteps, 'String', {num2str(round(x_pos)), 'steps'});
    set(handles.verMoverTextMetric, 'String', [num2str(round(dy)) ' um']);
    set(handles.verMoverTextSteps, 'String', {num2str(round(y_pos)), 'steps'});

function updateMoverPlot_gauges(handles)
    %disp('updateMoverPlot_gauges')
    
    %Get the status of the gauges
    try
        [x,xu,xt] = getLastValidPos(handles.positionGaugeConnectorX);
        [y,yu,yt] = getLastValidPos(handles.positionGaugeConnectorY);
    catch e
        disp('lost gauges, retrying connection...')
        clear handles.positionGaugeConnectorX
        clear handles.positionGaugeConnectorY
        handles.positionGaugeConnectorX = positionGaugeConnector(config.gauge.host, config.gauge.xport);
        handles.positionGaugeConnectorY = positionGaugeConnector(config.gauge.host, config.gauge.yport);
        [x,xu,xt] = getLastValidPos(handles.positionGaugeConnectorX);
        [y,yu,yt] = getLastValidPos(handles.positionGaugeConnectorY);
    end
    
    set(handles.gaugeXpos, 'String', [num2str(x), ' ', xu]);
    set(handles.gaugeYpos, 'String', [num2str(y), ' ', yu]);
    
    if handles.lastGaugeTimeX ~= xt
        set(handles.gaugeXlbl, 'BackgroundColor', 'yellow')
        handles.lastGaugeTimeX = xt;
    end
    if handles.lastGaugeTimeY ~= yt
        set(handles.gaugeYlbl, 'BackgroundColor', 'yellow')
        handles.lastGaugeTimeY = yt;
    end
    pause(0.25/handles.posGaugeRefreshRate);
    set(handles.gaugeXlbl, 'BackgroundColor', 'green')
    set(handles.gaugeYlbl, 'BackgroundColor', 'green')
        
function [handles] = setupDischargePlot(handles)
    config = CONFIG();
    
    % set up the current plot
    axes(handles.dischargePlot);
    handles.dischargePlot_PMT = plot(NaN, NaN, '-', 'Color', 0.7*[1 1 1]);
    hold on;
    handles.dischargePlot_out = plot(NaN, NaN, '-', 'Color', [0.85 0.325 0.098]);
    handles.dischargePlot_in = plot(NaN, NaN, '-', 'Color', [0 0.447 0.741]);
    hold off;
    ylim(config.history.current_limits);
    xlabel('Time (ns)');
    ylabel('Discharge current (A)');
    title('Compact Marx Bank current', 'FontSize', 7);
    legend([handles.dischargePlot_in, handles.dischargePlot_out, handles.dischargePlot_PMT], ...
           {'Ingoing', 'Outgoing', 'PMT (a.u.)'}, ...
           'location', 'southeast', ...
           'EdgeColor', 'none', 'Color', 'none', 'FontSize', 7);

       
function [handles] = setupDischargeHistoryPlots(handles)
    config = CONFIG();
    lim = config.history.current_limits;
    
    % set up the history waterfall plots
    axes(handles.dischargeHistoryIn);
    colormap(handles.dischargeHistoryIn, cmap(lim(1), lim(2)));
    handles.dischargeHistoryIn_waterfall = imagesc(zeros(1));
    set(gca, 'XTickLabel', []);
    ylabel('Time (ns)');
    caxis(lim);
    set(gca, 'ydir', 'normal');
    title('Into the plasma lens', 'FontSize', 7);
    
    axes(handles.dischargeHistoryOut);
    colormap(handles.dischargeHistoryOut, cmap(lim(1), lim(2)));
    colormap(cmap(lim(1), lim(2)));
    handles.dischargeHistoryOut_waterfall = imagesc(zeros(1));
    xlabel('Shots');
    ylabel('Time (ns)');
    caxis(lim);
    set(gca, 'ydir', 'normal');
    title('Out of the plasma lens', 'FontSize', 7);
    

function updateDischargePlot(handles, shot)
    config = CONFIG();
    
    % get scopes
    [ts_PMT, vs_PMT] = getScope(config.scopes.PMT, shot, true);
    [ts_in, vs_in] = getScope(config.scopes.current_in, shot, true);
    [ts_out, vs_out] = getScope(config.scopes.current_out, shot, true);

    % update the timing limits
    t_min = min([min(ts_PMT) min(ts_in) min(ts_out)]);
    t_max = max([max(ts_PMT) max(ts_in) max(ts_out)]);
    set(handles.dischargePlot, 'XLim', [0, t_max-t_min]);
    
    % update the scope plot
    set(handles.dischargePlot_PMT, 'XData', ts_PMT-t_min, 'YData', vs_PMT*config.scopes.PMT_fudgefactor);
    set(handles.dischargePlot_in, 'XData', ts_in-t_min, 'YData', vs_in*config.scopes.cal_AperV_in);
    set(handles.dischargePlot_out, 'XData', ts_out-t_min, 'YData', vs_out*config.scopes.cal_AperV_out);
    
    
function updateDischargeHistoryPlots(handles, shot)
    config = CONFIG();
    
    % incoming current
    [ts_in, vs_in] = getScope(config.scopes.current_in, shot, true);
    ts_in = ts_in - min(ts_in);
    history_in = handles.dischargeHistoryIn_waterfall.CData;
    if numel(ts_in) ~= size(history_in, 1)
        history_in = zeros(numel(ts_in), 1);
    end
    nextline_in = vs_in * config.scopes.cal_AperV_in; % calibrating
    if length(nextline_in) < size(history_in,1)
        tmp = zeros(1,size(history_in,1));
        tmp(1:length(nextline_in)) = nextline_in;
        nextline_in = tmp;
    end
    history_in = [nextline_in', history_in];
    if size(history_in, 2) > config.history.max_shots
        history_in = history_in(:, 1:(end-1));
    end
    set(handles.dischargeHistoryIn_waterfall, 'CData', history_in);
    set(handles.dischargeHistoryIn_waterfall, 'YData', ts_in);
    set(handles.dischargeHistoryIn, 'XLim', [1 size(history_in, 2)]-0.5, 'YLim', [min(ts_in) max(ts_in)]);
    
    % outgoing current
    [ts_out, vs_out] = getScope(config.scopes.current_out, shot, true);
    ts_out = ts_out-min(ts_out);
    history_out = handles.dischargeHistoryOut_waterfall.CData;
    if numel(ts_out) ~= size(history_out, 1)
        history_out = zeros(numel(ts_out), 1);
    end
    nextline_out = vs_out * config.scopes.cal_AperV_out; % calibrating
    if length(nextline_out) < size(history_out,1)
        tmp = zeros(1,size(history_out,1));
        tmp(1:length(nextline_out)) = nextline_out;
        nextline_out = tmp;
    end
    history_out = [nextline_out', history_out];
    if size(history_out, 2) > config.history.max_shots
        history_out = history_out(:, 1:(end-1));
    end
    set(handles.dischargeHistoryOut_waterfall, 'CData', history_out);
    set(handles.dischargeHistoryOut_waterfall, 'YData', ts_out);
    set(handles.dischargeHistoryOut, 'XLim', [1 size(history_out, 2)]-0.5, 'YLim', [min(ts_out) max(ts_out)]);
    
    
function updateDischargeStatus(handles)
    config = CONFIG();
    
    % update ON/OFF status text
    isDischarging = getVariable(config.trigger.switch);
    if isDischarging
        % if discharging
        set(handles.dischargeStatusText, 'String', 'SPARKING');
        
        % blink the background color
        sec = floor(mod(now,0.0001/1.2)*1.2e5);
        if mod(sec,2)
            bgColor = 0.94*[1 1 1]; % standard gray
        else
            bgColor = [1 1 0]; % bright yellow
        end
        set(handles.dischargeStatusText, 'BackgroundColor', bgColor);
    else
        % if off
        set(handles.dischargeStatusText, 'String', 'OFF');
        set(handles.dischargeStatusText, 'BackgroundColor', 0.94*[1 1 1]);
    end
    
    % update trigger timing text
    triggerTiming = getVariable(config.trigger.timing);
    set(handles.dischargeTriggerTimingText, 'String', [num2str(triggerTiming) ' ns'])
    
    
function [handles] = setupCameraPlots(handles) 
    % set up all cameras
    camPlots = {'capillaryCamPlot', 'electrodeCamPlot', 'plasmaOTRcamPlot', 'inAirYAGcamPlot'};
    for i = 1:numel(camPlots)
        axes(handles.(camPlots{i})); %#ok<LAXES>
        colormap(handles.(camPlots{i}), cmap(-1, 1));
        handles.([camPlots{i} '_image']) = imagesc(zeros(100));
        set(gca, 'XTickLabel', [], 'YTickLabel', []);
        handles.(camPlots{i}).XAxis.Color = 'none';
        handles.(camPlots{i}).YAxis.Color = 'none';
        axis tight;
    end

    
    
%% UPDATE THE CAMERAS EVERY SHOT

function updateCameras(handles, shot, digital)
    config = CONFIG();
    
    % update all the cameras
    if ~digital
        camPlots = {'capillaryCamPlot', 'electrodeCamPlot'};
        camNames = {'capillary', 'electrode_OTR'};
        scaleSliders = {'capillaryScaleSlider', 'electrodeScaleSlider'};
        autoScales = {'capillaryAutoScaleToggle', 'electrodeAutoScaleToggle'};
    else
        camPlots = {'plasmaOTRcamPlot', 'inAirYAGcamPlot'};
        camNames = {'plasma_OTR', 'in_air_YAG'};
        scaleSliders = {'plasmaOTRscaleSlider', 'inAirYAGscaleSlider'};
        autoScales = {'plasmaOTRautoScaleToggle', 'inAirYAGautoScaleToggle'};
    end
    
    
    %disp('updateCameras')
    for i = 1:numel(camPlots)
        ax = handles.(camPlots{i});
        img = getImage(config.cams.(camNames{i}), config.digicams.(camNames{i}), shot);
        if ~isnan(img)
            % turn on the plot
            set(handles.([camPlots{i} '_image']), 'Visible', 'on');
            set(handles.(camPlots{i}), 'Visible', 'on');
            
            % subtract background
            bg_img = handles.(camPlots{i}).UserData;
            if size(img) == size(bg_img)
                img = img - bg_img; % subtract the background (if taken)
            end
            
            % update image
            set(handles.([camPlots{i} '_image']), 'CData', img);
            
            % determine scale
            if handles.(autoScales{i}).Value
                img_mf = medfilt2(img, 5*[1 1]);
                cm = max(abs(max(max(img_mf))), abs(-min(min(img_mf))));
            else
                cm = handles.(scaleSliders{i}).Value * 2500;
            end
            set(handles.(autoScales{i}), 'String', num2str(round(cm)));
            caxis(ax, [-cm cm]);
            axis(ax,'equal');
        else
            set(handles.([camPlots{i} '_image']), 'Visible', 'off');
            set(handles.(camPlots{i}), 'Visible', 'off');
        end
    end
    
    
    
%% UPDATE THE GATED CAMERA TRIGGER STATUS TEXT

function updateGatedCameraStatus(handles)
    config = CONFIG();
    
    % update ON/OFF status text
    gateEnabled = getVariable(config.camgate.switch);
    if gateEnabled
        bgColor = [0 0 0]; % if triggering
    else
        bgColor = [1 0 0]; % if off
    end
    set(handles.gatedCamTriggerTimingText, 'ForegroundColor', bgColor);
    
    % update trigger timing text
    triggerTiming = getVariable(config.camgate.timing);
    set(handles.gatedCamTriggerTimingText, 'String', [num2str(triggerTiming) ' ns'])

    
    
%% TURN DISCHARGE ON/OFF WITH A TOGGLE BUTTON

function dischargeOnOffToggle_Callback(hObject, eventdata, handles)
    config = CONFIG();
    if hObject.Value
        turnDischargeON(); % switch the trigger on (if ON button is pressed)
        set(hObject, 'String', 'Turn OFF'); % update the button
        set(hObject, 'BackgroundColor', [0.9 0.225 0.098]);
    else
        turnDischargeOFF(); % switch the trigger off
        set(hObject, 'String', 'Turn ON'); % update the button
        set(hObject, 'BackgroundColor', 0.94*[1 1 1]);
    end

function turnDischargeOFF()
    config = CONFIG();
    if getVariable(config.trigger.switch) == true
        setVariable(config.trigger.switch, false)
    end

function turnDischargeON()
    config = CONFIG();
    if getVariable(config.trigger.switch) == false
        setVariable(config.trigger.switch, true)
    end
    
    
%% PLASMA OTR SCREEN IN/OUT TOGGLE BUTTON

function plasmaOTRscreenInOutToggle_Callback(hObject, eventdata, handles)
    config = CONFIG();
    if get(hObject,'Value')
        set(hObject, 'String', 'Put screen OUT');
        screenSelect = true;
    else
        set(hObject, 'String', 'Put screen IN');
        screenSelect = false;
    end
    setVariable([config.cams.plasma_OTR '/Setting#screenSelect'], screenSelect)

    
    
%% MOVE THE PLASMA LENS MOVER VERTICALLY

% function [] = moveHorizontal(dx)
%     config = CONFIG();
%     x_target = getVariable(config.mover.hor_target);
%     setVariable(config.mover.hor_request, x_target+dx*config.mover.cal_x)
%     
% function moveLeftButton10um_Callback(~, ~, ~); moveHorizontal(-10)
% function moveLeftButton100um_Callback(~, ~, ~); moveHorizontal(-100)
% function moveLeftButton1mm_Callback(~, ~, ~); moveHorizontal(-1000)
% function moveRightButton10um_Callback(~, ~, ~); moveHorizontal(10)
% function moveRightButton100um_Callback(~, ~, ~); moveHorizontal(100)
% function moveRightButton1mm_Callback(~, ~, ~); moveHorizontal(1000)

%GB edit
function [] = moveHorizontal(dx,cal)

    config = CONFIG();
    if ~exist('cal','var')
        cal = config.mover.cal_x;
    end
    x_target = getVariable(config.mover.hor_target);
    setVariable(config.mover.hor_request, x_target+dx*cal)    
    
function moveLeftButton10um_Callback(~, ~, ~); config = CONFIG(); moveHorizontal(-10, config.mover.cal_x_10um)
function moveLeftButton50um_Callback(~, ~, ~); config = CONFIG(); moveHorizontal(-50, config.mover.cal_x_50um)
function moveLeftButton1mm_Callback(~, ~, ~); config = CONFIG(); moveHorizontal(-1000, config.mover.cal_x_50um) %largest unit calibrated = 50um
function moveRightButton10um_Callback(~, ~, ~); config = CONFIG(); moveHorizontal(10, config.mover.cal_x_10um)
function moveRightButton50um_Callback(~, ~, ~); config = CONFIG(); moveHorizontal(50, config.mover.cal_x_50um)
function moveRightButton1mm_Callback(~, ~, ~); config = CONFIG(); moveHorizontal(1000, config.mover.cal_x_50um) %largest unit calibrated = 50um


%% MOVE THE PLASMA LENS MOVER VERTICALLY

% function moveVertical(dy)
%     config = CONFIG();
%     y_target = getVariable(config.mover.vert_target);
%     setVariable(config.mover.vert_request, y_target+dy*config.mover.cal_y)
%     
% function moveUpButton10um_Callback(~, ~, ~); moveVertical(10); % [um]
% function moveUpButton100um_Callback(~, ~, ~); moveVertical(100); % [um]
% function moveDownButton10um_Callback(~, ~, ~); moveVertical(-10); % [um]
% function moveDownButton100um_Callback(~, ~, ~); moveVertical(-100); % [um]

%GB edit
function moveVertical(dy,cal)
    config = CONFIG();
    if ~exist('cal','var')
        cal = config.mover.cal_y;
    end
    y_target = getVariable(config.mover.vert_target);
    setVariable(config.mover.vert_request, y_target+dy*cal)
    
function moveUpButton10um_Callback(~, ~, ~); config = CONFIG(); moveVertical(10, config.mover.cal_y_10um); % [um]
function moveUpButton50um_Callback(~, ~, ~); config = CONFIG(); moveVertical(50, config.mover.cal_y_50um); % [um]
function moveDownButton10um_Callback(~, ~, ~); config = CONFIG(); moveVertical(-10, config.mover.cal_y_10um); % [um]
function moveDownButton50um_Callback(~, ~, ~); config = CONFIG(); moveVertical(-50, config.mover.cal_y_50um); % [um]


%% MOVE CAPILLARY IN, OUT or to CENTER

function moveOutButton_Callback(~, ~, ~)
    config = CONFIG();
    setVariable(config.mover.hor_request, config.mover.x_ref_out);
    setVariable(config.mover.vert_request, config.mover.y_ref);

function moveInButton_Callback(~, ~, handles)
    config = CONFIG();
    setVariable(config.mover.hor_request, handles.moverReference.x_ref);

function centerLensButton_Callback(~, ~, handles)
    config = CONFIG();
    setVariable(config.mover.hor_request, handles.moverReference.x_ref);
    setVariable(config.mover.vert_request, handles.moverReference.y_ref);
    

%% UPDATE THE MOVER REFERENCE POSITION
% set the horizontal reference
function moverHorRefEdit_Callback(hObject, ~, handles)
    handles.moverReference.x_ref = str2double(hObject.String);
    guidata(hObject, handles);

function moverHorRefEdit_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

    
% set the vertical reference
function moverVertRefEdit_Callback(hObject, ~, handles)
    handles.moverReference.y_ref = str2double(hObject.String);
    guidata(hObject, handles);
    
function moverVertRefEdit_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

    
    
%% SET UP THE PERIODIC ON/OFF DISCHARGE SHOTS

function periodicNumShotsONedit_CreateFcn(hObject, eventdata, handles)
    config = CONFIG();
    set(hObject, 'String', num2str(config.periodic.numshots_on));
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

function periodicNumShotsOFFedit_CreateFcn(hObject, eventdata, handles)
    config = CONFIG();
    set(hObject, 'String', num2str(config.periodic.numshots_off));
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

    
    
%% SET THE PERIODIC COUNTER TO 1 AT STARTUP

function periodicOnOffPanel_CreateFcn(hObject, eventdata, handles)
    set(hObject, 'UserData', 1);

    
    
%% CHANGE THE DISCHARGE TRIGGER TIMING

function changeTriggerTiming(dt)
    config = CONFIG();
    t0 = getVariable(config.trigger.timing);
    setVariable(config.trigger.timing, t0+dt);
    
function timingEarlierButton4ns_Callback(~, ~, ~); changeTriggerTiming(-4);
function timingEarlierButton20ns_Callback(~, ~, ~); changeTriggerTiming(-20);
function timingEarlierButton100ns_Callback(~, ~, ~); changeTriggerTiming(-100);
function timingLaterButton4ns_Callback(~, ~, ~); changeTriggerTiming(4);
function timingLaterButton20ns_Callback(~, ~, ~); changeTriggerTiming(20);
function timingLaterButton100ns_Callback(~, ~, ~); changeTriggerTiming(100);



%% UPDATE THE HORIZONTAL AND VERTICAL MOVER REFERENCE POSITION

function updateHorRefHereButton_Callback(hObject, eventdata, handles)
    config = CONFIG();
    x_actual = round(getVariable(config.mover.hor_actual));
    handles.moverReference.x_ref = x_actual;
    set(handles.moverHorRefEdit, 'String', num2str(x_actual));
    guidata(hObject, handles);
    
function updateVertRefHereButton_Callback(hObject, eventdata, handles)
    config = CONFIG();
    y_actual = round(getVariable(config.mover.vert_actual));
    handles.moverReference.y_ref = y_actual;
    set(handles.moverVertRefEdit, 'String', num2str(y_actual));
    guidata(hObject, handles);
    

    
%% CAMERA ON/OFF TOGGLE BUTTONS

function turnOnOffCamera(hObject, camera)
    config = CONFIG();
    if hObject.Value % turn on the camera
        set(hObject, 'String', 'Turn OFF');
        camSwitch = true;
    else % turn off the camera
        set(hObject, 'String', 'Turn ON');
        camSwitch = false;
    end
    %setVariable([config.cams.(camera), '/Setting#cameraSwitch'], camSwitch);
    matlabJapc.staticSetSignal('', [config.cams.(camera), '/Setting#cameraSwitch'], int32(camSwitch));

function capillaryCamOnOffToggle_Callback(hObject, ~, ~); turnOnOffCamera(hObject, 'capillary');
function electrodeCamOnOffToggle_Callback(hObject, ~, ~); turnOnOffCamera(hObject, 'electrode_OTR');
function plasmaOTRcamOnOffToggle_Callback(hObject, ~, ~); turnOnOffCamera(hObject, 'plasma_OTR');
function inAirYAGcamOnOffToggle_Callback(hObject, ~, ~); turnOnOffCamera(hObject, 'in_air_YAG');
    


%% UPDATE CAMERA ON/OFF TOGGLES

function updateAllCameraOnOffToggles(handles)
    updateCameraOnOffToggle(handles.capillaryCamOnOffToggle, 'capillary');
    updateCameraOnOffToggle(handles.electrodeCamOnOffToggle, 'electrode_OTR');
    updateCameraOnOffToggle(handles.plasmaOTRcamOnOffToggle, 'plasma_OTR');
    updateCameraOnOffToggle(handles.inAirYAGcamOnOffToggle, 'in_air_YAG');
    
function updateCameraOnOffToggle(hObject, camera)
    config = CONFIG(); 
    camSwitch = getVariable([config.cams.(camera) '/Setting#cameraSwitch']);
    if strcmp(camSwitch, 'ON') || strcmp(camSwitch, 'CCD_ON_RAD_STDBY')
        set(hObject, 'Value', 1);
        set(hObject, 'String', 'Turn OFF');
    elseif strcmp(camSwitch, 'OFF')
        set(hObject, 'Value', 0);
        set(hObject, 'String', 'Turn ON');
    end

    
    
%% CHANGE THE CAMERA GAINS

function setGain(camera, hObject)
    config = CONFIG();
    %currentValue = find(strcmpi(config.gains.vals, config.gains.vals(hObject.Value)));
 
    %setVariable([config.cams.(camera) '/Setting#videoGain'], currentValue);
    %disp(getVariable([config.cams.(camera) '/Setting#videoGain']));
    %disp(hObject.Value)
    setVariable([config.cams.(camera) '/Setting#videoGain'], int32(hObject.Value-1));
    
    
function inAirYAGcamGainMenu_Callback(hObject, ~, ~); setGain('in_air_YAG', hObject);
function plasmaOTRcamGainMenu_Callback(hObject, ~, ~); setGain('plasma_OTR', hObject);
function electrodeCamGainMenu_Callback(hObject, ~, ~); setGain('electrode_OTR', hObject);
function capillaryCamGainMenu_Callback(hObject, ~, ~); setGain('capillary', hObject);


    
%% CREATE THE GAIN MENUS

function populateGainMenu(hObject)
    config = CONFIG();
    set(hObject, 'String', config.gains.text);
    
function inAirYAGcamGainMenu_CreateFcn(hObject, ~, ~); populateGainMenu(hObject);
function plasmaOTRcamGainMenu_CreateFcn(hObject, ~, ~); populateGainMenu(hObject);
function electrodeCamGainMenu_CreateFcn(hObject, ~, ~); populateGainMenu(hObject);
function capillaryCamGainMenu_CreateFcn(hObject, ~, ~); populateGainMenu(hObject);


    
%% UPDATE THE GAIN MENUS
    
function updateGainMenus(handles)
    updateGainMenu(handles.capillaryCamGainMenu, 'capillary');
    updateGainMenu(handles.electrodeCamGainMenu, 'electrode_OTR');
    updateGainMenu(handles.plasmaOTRcamGainMenu, 'plasma_OTR');
    updateGainMenu(handles.inAirYAGcamGainMenu, 'in_air_YAG');
    
function updateGainMenu(hObject, camera)
    config = CONFIG();
    gain = getVariable([config.cams.(camera) '/Setting#videoGain']);
    %currentValue = find(strcmpi(config.gains.vals, gain));
    %set(hObject, 'Value', currentValue);
    set(hObject, 'Value', gain+1);

    
    
%% TURN CAMERA LIGHTS ON/OFF

function lampSwitch(hObject, camera)
    config = CONFIG();
    if hObject.Value
        lampSwitch = true;
        set(hObject, 'String', 'Turn LAMP OFF');
    else
        lampSwitch = false;
        set(hObject, 'String', 'Turn LAMP ON');
    end
    setVariable([config.cams.(camera) '/Setting#lampSwitch'], lampSwitch);

function capillaryLampOnOffToggle_Callback(hObject, ~, ~); lampSwitch(hObject, 'capillary') %#ok<*DEFNU>
function electrodeLampOnOffToggle_Callback(hObject, ~, ~); lampSwitch(hObject, 'electrode_OTR')
function plasmaOTRlampOnOffToggle_Callback(hObject, ~, ~); lampSwitch(hObject, 'plasma_OTR')
function inAirYAGlampOnOffToggle_Callback(hObject, ~, ~); lampSwitch(hObject, 'in_air_YAG')


%% UPDATE THE CAMERA LAMP TOGGLE BUTTONS

function updateAllLampOnOffToggles(handles)
    updateLampOnOffToggle(handles.capillaryLampOnOffToggle, 'capillary');
    updateLampOnOffToggle(handles.electrodeLampOnOffToggle, 'electrode_OTR');
    updateLampOnOffToggle(handles.plasmaOTRlampOnOffToggle, 'plasma_OTR');
    updateLampOnOffToggle(handles.inAirYAGlampOnOffToggle, 'in_air_YAG');
    
function updateLampOnOffToggle(hObject, camera)
    config = CONFIG(); 
    % update lamp switch
    lampSwitch = getVariable([config.cams.(camera) '/Setting#lampSwitch']);
    if strcmp(lampSwitch, 'ON')
        set(hObject, 'Value', 1);
        set(hObject, 'String', 'Turn LAMP OFF');
    elseif strcmp(lampSwitch, 'OFF')
        set(hObject, 'Value', 0);
        set(hObject, 'String', 'Turn LAMP ON');
    end
    
    
    
%% UPDATE THE CAMERA LAMP BRIGHTNESS SLIDERS

function updateAllLampSliders(handles)
    updateLampSlider(handles.capillaryCamLampSlider, 'capillary');
    updateLampSlider(handles.electrodeLampSlider, 'electrode_OTR');
    updateLampSlider(handles.plasmaOTRcamLampSlider, 'plasma_OTR');
    updateLampSlider(handles.inAirYAGcamLampSlider, 'in_air_YAG');
    
    
function updateLampSlider(hObject, camera)  
    config = CONFIG(); 
    % adjust brightness slider
    brightness = getVariable([config.cams.(camera) '/Setting#' config.lamps.(camera)]);
    maxBright = getVariable([config.cams.(camera) '/Setting#' config.lamps.(camera) '_max']);
    minBright = getVariable([config.cams.(camera) '/Setting#' config.lamps.(camera) '_min']);
    brightnessNorm = (brightness-minBright)/(maxBright-minBright);
    set(hObject, 'Value', brightnessNorm);
    



%% BACKGROUND SAVING for cameras

function setBackground(carrierObject, camera, resetFlag)
    config = CONFIG();
    bg_img = getImage(config.cams.(camera), config.digicams.(camera));
    if exist('resetFlag','var') && resetFlag
        bg_img = []; 
    end
    set(carrierObject, 'UserData', bg_img);
    
% set backgrounds
function capillaryCamBgButton_Callback(~, ~, handles); setBackground(handles.capillaryCamPlot, 'capillary');
function electrodeCamBgButton_Callback(~, ~, handles); setBackground(handles.electrodeCamPlot, 'electrode_OTR');
function plasmaOTRcamBgButton_Callback(~, ~, handles); setBackground(handles.plasmaOTRcamPlot, 'plasma_OTR');
function inAirYAGcamBgButton_Callback(~, ~, handles); setBackground(handles.inAirYAGcamPlot, 'in_air_YAG');

% reset backgrounds
function capillaryCamBgResetButton_Callback(~, ~, handles); setBackground(handles.capillaryCamPlot, 'capillary', true);
function electrodeCamBgResetButton_Callback(~, ~, handles); setBackground(handles.gatedCamPlot, 'electrode_OTR', true);
function plasmaOTRcamBgResetButton_Callback(~, ~, handles); setBackground(handles.plasmaOTRcamPlot, 'plasma_OTR', true);
function inAirYAGcamBgResetButton_Callback(~, ~, handles); setBackground(handles.inAirYAGcamPlot, 'in_air_YAG', true);


%% SET UP LAMP BRIGHTNESS SLIDERS

function inAirYAGcamLampSlider_CreateFcn(hObject, eventdata, handles)
function plasmaOTRcamLampSlider_CreateFcn(hObject, eventdata, handles)
function capillaryCamLampSlider_CreateFcn(hObject, eventdata, handles)



%% ADJUST LAMP BRIGHTNESS

function adjustLampBrightness(hObject, camera)
    config = CONFIG();
    brightness = round(1000*hObject.Value);
        %GB edit
        brightness = abs(brightness);    
    setVariable([config.cams.(camera) '/Setting#' config.lamps.(camera)], brightness);
    
function capillaryCamLampSlider_Callback(hObject, ~, ~); adjustLampBrightness(hObject, 'capillary');
function electrodeLampSlider_Callback(hObject, ~, ~); adjustLampBrightness(hObject, 'electrode_OTR');
function plasmaOTRcamLampSlider_Callback(hObject, ~, ~); adjustLampBrightness(hObject, 'plasma_OTR');
function inAirYAGcamLampSlider_Callback(hObject, ~, ~); adjustLampBrightness(hObject, 'in_air_YAG');
    


%% CHANGE THE GATED CAMERA TRIGGER TIMING
% will not be used until a gated camera is added back again
%{
function changeGateTriggerTiming(dt)
    config = CONFIG();
    t0 = getVariable(config.camgate.timing);
    setVariable(config.camgate.timing, t0+dt);
    setVariable(config.camgate.switch, true);
    
function gateTriggerEarlier100nsButton_Callback(~, ~, ~); changeGateTriggerTiming(-100);
function gateTriggerEarlier20nsButton_Callback(~, ~, ~); changeGateTriggerTiming(-20);
function gateTriggerEarlier4nsButton_Callback(~, ~, ~); changeGateTriggerTiming(-4);
function gateTriggerLater4nsButton_Callback(~, ~, ~); changeGateTriggerTiming(4);
function gateTriggerLater20nsButton_Callback(~, ~, ~); changeGateTriggerTiming(20);
function gateTriggerLater100nsButton_Callback(~, ~, ~); changeGateTriggerTiming(100);
%}


%% AUTO SCALE TOGGLE BUTTON

function autoScaleToggler(hObject, handles, sliderName)
    if hObject.Value
        set(handles.(sliderName), 'Enable', 'off')
    else
        set(handles.(sliderName), 'Enable', 'on')
    end
    
function capillaryAutoScaleToggle_Callback(hObject, ~, handles); autoScaleToggler(hObject, handles, 'capillaryScaleSlider')
function electrodeAutoScaleToggle_Callback(hObject, ~, handles); autoScaleToggler(hObject, handles, 'electrodeScaleSlider')
function plasmaOTRautoScaleToggle_Callback(hObject, ~, handles); autoScaleToggler(hObject, handles, 'plasmaOTRscaleSlider')
function inAirYAGautoScaleToggle_Callback(hObject, ~, handles);  autoScaleToggler(hObject, handles, 'inAirYAGscaleSlider')



function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit18 as text
%        str2double(get(hObject,'String')) returns contents of edit18 as a double


% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit19 as text
%        str2double(get(hObject,'String')) returns contents of edit19 as a double


% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton70.
function pushbutton70_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton70 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton71.
function pushbutton71_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton71 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton72.
function pushbutton72_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton72 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton73.
function pushbutton73_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton73 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton74.
function pushbutton74_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton74 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton75.
function pushbutton75_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton75 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton76.
function pushbutton76_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton76 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton77.
function pushbutton77_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton77 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton54.
function pushbutton54_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton54 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton55.
function pushbutton55_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton55 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton56.
function pushbutton56_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton56 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton57.
function pushbutton57_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton57 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit15 as text
%        str2double(get(hObject,'String')) returns contents of edit15 as a double


% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton58.
function pushbutton58_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton58 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton59.
function pushbutton59_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton59 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton60.
function pushbutton60_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton60 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton61.
function pushbutton61_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton61 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double


% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton62.
function pushbutton62_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton62 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton63.
function pushbutton63_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton63 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton64.
function pushbutton64_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton64 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton65.
function pushbutton65_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton65 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double


% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton66.
function pushbutton66_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton66 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton67.
function pushbutton67_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton67 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton68.
function pushbutton68_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton68 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton69.
function pushbutton69_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton69 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function checkbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2
    toggleState = get(hObject,'Value');
    if toggleState == 1
        matlabJapc.staticSetSignal('','CA.PLASMA_CHARGING/Setting#enumValue','ON')
    else
        matlabJapc.staticSetSignal('','CA.PLASMA_CHARGING/Setting#enumValue','OFF')
    end

% --- Executes during object creation, after setting all properties.
function checkbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    if strcmp(matlabJapc.staticGetSignal('','CA.PLASMA_CHARGING/Setting#enumValue'), 'ON')
        set(hObject,'Value',1);
    else
        set(hObject,'Value',0);
    end
