function map = cmap(mn,mx)

    % determine relative position of the zero
    if nargin < 1
        zero = 0.5;
    else
        zero = -mn/(mx-mn);
    end
    
    % COLORS
    %{
    if zero == 0 % no plasma
        map = interp1([0, 0.5, 1],[255 255 255; 70 172 255; 0 51 139]/256,linspace(0, 1, 500));
    elseif zero == 1 % no beam
        map = interp1([0, 0.5, 1],[255,148,0; 255 197 124; 255 255 255]/256,linspace(0, 1, 500));
    else
        map = interp1([0 mean([0,zero]) zero mean([zero,1]) 1],[255,148,0; 255 197 124; 255 255 255; 70 172 255; 0 51 139]/256,linspace(0, 1, 500));
    end
    %}
    
    % NEW COLORS
    %col1 = [70 172 255];
    %col2 = [0 51 139];
    col_zero = [0 0 0];
    col_min = [255 10 10];
    col_halfmin = (col_min + col_zero)/2;
    col_max = [255 255 255];
    col_halfmax = [100 172 200]; % (col_max + col_zero)/2;
    if zero == 0 % no plasma
        map = interp1([0, 0.5, 1],[col_zero; col_halfmax; col_max]/256,linspace(0, 1, 500));
    elseif zero == 1 % no beam
        map = interp1([0, 0.5, 1],[col_min; col_halfmin; col_zero]/256,linspace(0, 1, 500));
    else
        map = interp1([0 mean([0,zero]) zero mean([zero,1]) 1],[col_min; col_halfmin; col_zero; col_halfmax; col_max]/256,linspace(0, 1, 500));
    end
    
end

