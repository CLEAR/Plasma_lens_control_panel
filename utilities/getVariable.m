function [ value ] = getVariable( name )

    dst = name(1:4);
    if strcmp(dst, 'CA.Q') || strcmp(dst, 'CA.D') || strcmp(dst, 'CA.B') || strcmp(dst, 'CL.E') || strcmp(name(1:6), 'CA.MOV')
        % read magnets
        value = matlabJapc.staticGetSignal('', name);
    else
        % read all other things
        value = matlabJapc.staticGetSignal('SCT.USER.ALL', name);
    end
end

