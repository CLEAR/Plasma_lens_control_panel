function [ tAxis, voltageAxis ] = getScope( name, shot, singleSubShot )
    
    % decide to get or decode based on inputs
    if ~exist('shot', 'var')
        % get scope signals if no shot provided
        warning('Using staticGetSignal() for scope %s',name);
        scope = matlabJapc.staticGetSignal('SCT.USER.SETUP', [name '/Acquisition']);
    else
        % otherwise decode the shot
        name = strrep(name, '.', '_');
        scope = shot.(name).Acquisition.value;
    end
    
    % voltage axis
    voltageAxis = double(scope.value)*scope.sensitivity + scope.offset; % [V]
    if singleSubShot
        voltageAxis = voltageAxis(1,:);
    end
    
    % time axis
    samples = 0:(numel(voltageAxis)-1);
    tAxis = samples*scope.sampleInterval + scope.firstSampleTime; % [ns]
    
end

