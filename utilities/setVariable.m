function [] = setVariable( name, value )
    
    dst = name(1:4);
    if strcmp(dst, 'CA.Q') || strcmp(dst, 'CA.D')
        % set magnets
        %warning('17/06/2019: force setting floats')
        matlabJapc.staticSetSignal('', name, value);
    elseif strcmp(name(1:9), 'CL.ECLCNT')
        if strcmp(name(end-6:end),'enabled')
            matlabJapc.staticSetSignal('', name, boolean(value));
        else
            matlabJapc.staticSetSignal('', name, int64(value));
        end
    elseif strcmp(name(end-11:end), 'screenSelect') || strcmp(name(end-9:end), 'lampSwitch')
        matlabJapc.staticSetSignal('', name, int32(value));
    elseif strcmp(name(end-3:end), 'Lamp')
        matlabJapc.staticSetSignal('', name, int16(value));
    elseif strcmp(name(1:6), 'CA.MOV')
        matlabJapc.staticSetSignal('', name, single(value));
    else
        % set all other things
        %matlabJapc.staticSetSignal('SCT.USER.ALL', name, value);
        matlabJapc.staticSetSignal('', name, value);
    end
end
