function [image, xaxis, yaxis] = getImage(name, digiName, shot)
    
    isDigital = false;
    if ~strcmp(digiName, 'N/A')
        isDigital = true;
    end
    
    try
        % decide to get or decode based on inputs
        if ~exist('shot', 'var')
            % get image signals if no shot provided
            warning('Using staticGetSignal() for camera %s / %s',name, digiName);
            if isDigital
                camera = matlabJapc.staticGetSignal('', [digiName '/ExtractionImage']);
            else
                camera = matlabJapc.staticGetSignal('SCT.USER.ALL', [name '/Image']);
            end
        else
            % otherwise decode the shot
            name = strrep(name, '.', '_');
            if isDigital
                digiName = strrep(digiName, '.', '_');
                camera = shot.(digiName).ExtractionImage.value;
            else
                camera = shot.(name).Image.value;
            end
        end
        
        % convert to a matrix
        if isDigital
            image = camera.image2D;
            gain = 1;
        else
            image = reshape(camera.imageSet, camera.nbPtsInSet1, camera.nbPtsInSet2)';
            % normalize gain
            switch camera.videoGain
                case 'X_ONE'
                    gain = 1;
                case 'X_TWO_DOT_TWO'
                    gain = 2.2;
                case 'X_SIX_DOT_FIVE'
                    gain = 6.5;
                case 'X_EIGHT'
                    gain = 8;
                otherwise
                    gain = 1;
            end
            image = image / gain;
        end
        
    catch
        % if no image, return a NaN
        image = NaN;
    end
    
end

